/**
 * 
 */
package br.com.poc.modules;

/**
 * @author wlmachado
 *
 */
public class Pessoa {
	
	private Integer idPessoa;
	private String nomPessoa;
	
	public Integer getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}
	public String getNomPessoa() {
		return nomPessoa;
	}
	public void setNomPessoa(String nomPessoa) {
		this.nomPessoa = nomPessoa;
	}

}
