package br.com.poc.app_resources;

import org.springframework.web.bind.annotation.RestController;

import br.com.poc.modules.Pessoa;

import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloResource {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
    
    @RequestMapping("/pessoa")
    public Pessoa getPessoa() {
    	Pessoa pessoa = new Pessoa();
    	pessoa.setNomPessoa("Thais Veloso de Carvalho");
    	pessoa.setIdPessoa(10);
    	return pessoa;
    }

}